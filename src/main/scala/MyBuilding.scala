trait Floor

trait Gender

case object Male extends Gender
case object Female extends Gender

trait Person{
  def age: Int
  def gender: Gender
}

trait LivingFloor extends Floor{
  def Guest1: Person
  def Guest2: Person
  def nextFloor: Floor
}

case object Attic extends Floor

case class Building(Address: String, firstFloor: Floor)

object MyBuilding {
  def main(args: Array[String]): Unit = {

    def protoFold(building: Building, acc0: Int)(f: (Int, LivingFloor) => Int): Int = {

    }

    def onFloorManOlderThen(acc: Int, floor: LivingFloor): Int{
      if()
    }

    def countOldManFloors(building: Building, olderThen: Int): Int = {

    }

    def womanMaxAge(building: Building):Int = {

    }



  }
}