sealed trait Tree

case class Node(value: Int, left: Tree, right: Tree) extends Tree

case object RedLeaf extends Tree
case object YellowLeaf extends Tree
case object GreenLeaf extends Tree

object MyTree extends App{

  def countYellowAndRedValues(tree: Tree): Int = {
    tree match{
      case Node(value, YellowLeaf | RedLeaf, right) => value + countYellowAndRedValues(right)
      case Node(value, left, YellowLeaf | RedLeaf) => value + countYellowAndRedValues(left)
      case Node(_, left, right) => countYellowAndRedValues(left) + countYellowAndRedValues(right)
      case _ => 0
    }
  }

  def maxValue(tree: Tree): Option[Int] = {
    tree match{
      case Node(value, _, YellowLeaf | RedLeaf | GreenLeaf) => Some(value)
      case Node(_, _, right) => maxValue(right)
      case _ => None
    }
  }

  /*              45
          35              58
      23      37      49      67
     G  R    G  G    Y  G    R  Y
  */

  val leftTree = Node(35, Node(23, GreenLeaf, RedLeaf), Node(37, GreenLeaf, GreenLeaf))
  val rightTree = Node(58, Node(49, YellowLeaf, GreenLeaf), Node(67, RedLeaf, YellowLeaf))
  val tree = Node(45, leftTree, rightTree)

  println(countYellowAndRedValues(tree))
  println(maxValue(tree))
}
